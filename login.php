<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/25/2017
 * Time: 10:50 PM
 */





include_once 'vendor/autoload.php';
$person=new \App\person\person();
session_start();

if(isset($_POST['submit'])){
    $person->setName($_POST['name']);
    $person->setPassword($_POST['password']);
    $person->login();
}

echo time();
if(isset($_SESSION['time'])){
    echo $_SESSION['time'];
    session_unset();
}

//session_start();
if(isset($_SESSION['fail'])){
    echo $_SESSION['fail'];
    session_unset();
}

if(isset($_SESSION['invalid'])){
    echo $_SESSION['invalid'];
    session_unset();
}

?>




<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<form action="" method="post">
    <label for="name">Name</label>
    <input type="text" name="name">

    <br/>
    <label for="password">Password</label>
    <input type="password" name="password">

    <input type="submit" name="submit">
</form>
</body>
</html>
