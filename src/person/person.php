<?php

/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/25/2017
 * Time: 10:26 PM
 */
namespace App\person;
use App\DB;

class person
{
    private $id;
    private $name;
    private $password;
    private $attem=0;
    private $timestamp;
    public $msg='';

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @param mixed $attemp
     */
    public function setAttemp($attemp)
    {
        $this->attemp = $attemp;
    }

    /**
     * @param mixed $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    public function login(){
        $sql="select * from login where name=:name";
        $stmt=DB::myQuery($sql);
        $stmt->bindValue(':name',$this->name);
        $stmt->execute();
        if ($stmt->rowCount()==1){
            $res=$stmt->fetch(\PDO::FETCH_OBJ);
            //$attepm=$res->attemp;
            if( $this->name==$res->name && $this->password==$res->password){
                if($res->timestamp>time()){
                    $_SESSION['time']="Your account has been disabled. Please try  5 minutes later!!";
                }else{
                    $sql="update login set timestamp=0 , attemp=0 where name=:name and password=:password";
                    $stmt=DB::myQuery($sql);
                    $stmt->bindValue(':name',$this->name);
                    $stmt->bindValue(':password',$this->password);
                    $stmt->execute();
                    session_start();
                    $_SESSION['loginsuccess']="login successful";
                    header('location:index.php');
                }
            }

            else{

                $res->attemp+=1;
                $sql="update login set attemp=:attemp where name=:name";
                $stmt=DB::myQuery($sql);
                $stmt->bindValue(':name',$this->name);
                $stmt->bindValue(':attemp',$res->attemp);
                $stmt->execute();

                if($res->attemp>3){
                    $time=time()+(60*5);
                    $sql="update login set timestamp='$time' where name=:name";
                    $stmt=DB::myQuery($sql);
                    $stmt->bindValue(':name',$this->name);
                    if($stmt->execute()){
                        $_SESSION['fail']="You have tried more than 3 times !! account is disabled for 5 minutes";

                    }
                }

            }
        }
        else{
            $_SESSION['invalid']="Invalid login";
        }

    }
}