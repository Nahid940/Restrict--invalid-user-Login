<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 9/25/2017
 * Time: 10:21 PM
 */
namespace App;
use PDO;
class DB{
    private static $pdo;

    public static function myCon(){
        try{
            self::$pdo=new \PDO('mysql:host=localhost;dbname=ok99',"root","");
        }catch (PDOException $exp){
            return $exp->getMessage();
        }
        return self::$pdo;
    }

    public static function myQuery($sql){
        return self::myCon()->prepare($sql);
    }
}


